package src.test.realitykings.Desktop.Original;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import static org.junit.Assert.assertTrue;

/**
 * Created by e_solar on 1/11/2017.
 */
public class firstTest {

    public WebDriver driver;

    @Before
    public void setup()
    {
       System.setProperty("webdriver.gecko.driver","C:\\gecko\\geckodriver.exe");
       DesiredCapabilities capabilities = DesiredCapabilities.firefox();
       capabilities.setCapability("marionette", true);

        driver = new FirefoxDriver();

//        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
//        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get("http://realitykings.com");
    }

    @After
    public void tearDown()
    {

        System.out.print(driver);
        driver.quit();
    }


    @Test
    public void print(){
        System.out.print("It worked!!!");

    }

    @Test
    public void print2(){
        System.out.print("It worked 3!!!");

    }

}
